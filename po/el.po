# Greek translation for gnome-connections.
# Copyright (C) 2020 gnome-connections's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-connections package.
# Efstathios Iosifidis <eiosifidis@gnome.org>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-connections gnome-3-38\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/connections/issues\n"
"POT-Creation-Date: 2023-05-16 10:45+0000\n"
"PO-Revision-Date: 2023-08-01 23:01+0300\n"
"Last-Translator: Efstathios Iosifidis <eiosifidis@gnome.org>\n"
"Language-Team: Greek <gnome-el-list@gnome.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.3.2\n"

#: data/org.gnome.Connections.appdata.xml.in:7
#: data/org.gnome.Connections.desktop.in:3 src/application.vala:92
#: src/ui/topbar.ui:9 src/ui/window.ui:5 src/window.vala:107
msgid "Connections"
msgstr "Συνδέσεις"

#: data/org.gnome.Connections.appdata.xml.in:8
msgid "View and use other desktops"
msgstr "Προβολή και χρήση άλλων υπολογιστών"

#: data/org.gnome.Connections.appdata.xml.in:10
msgid ""
"Connections allows you to connect to and use other desktops. This can be a "
"great way to access content or software on a different desktop operating "
"system. It can also be used as a way to provide support to users who might "
"need help."
msgstr ""
"Οι Συνδέσεις σας επιτρέπουν να συνδέεστε και να χρησιμοποιείτε άλλους "
"επιτραπέζιους υπολογιστές. Αυτός μπορεί να είναι ένας πολύ καλός τρόπος "
"πρόσβασης σε περιεχόμενο ή λογισμικό σε διαφορετικό λειτουργικό σύστημα "
"επιφάνειας εργασίας. Μπορεί επίσης να χρησιμοποιηθεί ως τρόπος παροχής "
"υποστήριξης σε χρήστες που μπορεί να χρειαστούν βοήθεια."

#: data/org.gnome.Connections.appdata.xml.in:13
msgid ""
"A range of different operating systems can be connected to, including Linux "
"and Windows desktops. You can also connect to virtual machines."
msgstr ""
"Μπορεί να συνδεθεί μια σειρά διαφορετικών λειτουργικών συστημάτων, "
"συμπεριλαμβανομένων των επιτραπέζιων υπολογιστών Linux και Windows. Μπορείτε "
"επίσης να συνδεθείτε σε εικονικές μηχανές."

#: data/org.gnome.Connections.appdata.xml.in:16
msgid ""
"Connections uses the widely supported VNC and RDP protocols, and one of "
"these must be enabled on the desktop that you want to connect to."
msgstr ""
"Οι συνδέσεις χρησιμοποιούν τα ευρέως υποστηριζόμενα πρωτόκολλα VNC και RDP "
"και ένα από αυτά πρέπει να είναι ενεργοποιημένο στον υπολογιστή στον οποία "
"θέλετε να συνδεθείτε."

#: data/org.gnome.Connections.appdata.xml.in:43
msgid "The GNOME Project"
msgstr "Το έργο GNOME"

#: data/org.gnome.Connections.desktop.in:8
msgid "vnc;rdp;remote;desktop;windows;support;access;view"
msgstr ""
"vnc;rdp;απομακρυσμένα;επιφάνεια εργασίας;windows;υποστήριξη;πρόσβαση;προβολή"

#: data/org.gnome.Connections.xml:5
msgid "Remote Desktop (VNC) file"
msgstr "Αρχείο απομακρυσμένης επιφάνειας εργασίας (VNC)"

#: src/actions-popover.vala:44
msgid "Delete"
msgstr "Διαγραφή"

#: src/actions-popover.vala:48 src/topbar.vala:66 src/ui/topbar.ui:220
msgid "Properties"
msgstr "Ιδιότητες"

#: src/application.vala:88
msgid "translator-credits"
msgstr ""
"Ελληνική μεταφραστική ομάδα GNOME\n"
" Ευστάθιος Ιωσηφίδης <iosifidis@opensuse.org>\n"
"\n"
"Για περισσότερες πληροφορίες, επισκεφθείτε τη σελίδα\n"
"http://gnome.gr/"

#: src/application.vala:89 src/application.vala:242
msgid "A remote desktop client for the GNOME desktop environment"
msgstr ""
"Ένας πελάτης απομακρυσμένης επιφάνειας εργασίας για το περιβάλλον επιφάνειας "
"εργασίας GNOME"

#: src/application.vala:151
#, c-format
msgid "Couldn’t open file of unknown mime type %s"
msgstr "Αδυναμία ανοίγματος αρχείου άγνωστου τύπου mime %s"

#: src/application.vala:182
#, c-format
msgid "Connection to “%s” has been deleted"
msgstr "Η σύνδεση στο «%s» διαγράφηκε"

#: src/application.vala:185
msgid "Undo"
msgstr "Aναίρεση"

#: src/application.vala:232
msgid "URL to connect"
msgstr "URL για σύνδεση"

#: src/application.vala:233
msgid "Open .vnc or .rdp file at the given PATH"
msgstr "Άνοιγμα αρχείου .vnc ή .rdp στο δοσμένο PATH"

#: src/application.vala:234
msgid "Open in full screen"
msgstr "Άνοιγμα σε πλήρη οθόνη"

#: src/application.vala:257
msgid "Too many command-line arguments specified.\n"
msgstr "Καθορίστηκαν πάρα πολλά ορίσματα γραμμής εντολών.\n"

#. Translators: %s => the timestamp of when the screenshot was taken.
#: src/connection.vala:70
#, c-format
msgid "Screenshot from %s"
msgstr "Στιγμιότυπο από %s"

#: src/connection.vala:85
msgid "Screenshot taken"
msgstr "Λήφθηκε στιγμιότυπο"

#. Translators: Open is a verb
#: src/connection.vala:88
msgid "Open"
msgstr "Άνοιγμα"

#: src/connection.vala:161
#, c-format
msgid "“%s” requires authentication"
msgstr "Το «%s» απαιτεί πιστοποίηση"

#: src/connection.vala:200
#, c-format
msgid "Authentication failed: %s"
msgstr "Η πιστοποίηση απέτυχε: %s"

#. Translators: Showing size of widget as WIDTH×HEIGHT here.
#: src/display-view.vala:126
#, c-format
msgid "%d×%d"
msgstr "%d×%d"

#: src/onboarding-dialog.vala:96 src/ui/onboarding-dialog.ui:148
msgid "_No Thanks"
msgstr "Ό_χι ευχαριστώ"

#: src/onboarding-dialog.vala:96
msgid "_Close"
msgstr "_Κλείσιμο"

#. Translators: Combo item for resizing remote desktop to window's size
#: src/rdp-preferences-window.vala:49 src/ui/vnc-preferences.ui:107
msgid "Resize desktop"
msgstr "Αλλαγή μεγέθους επιφάνειας εργασίας"

#: src/topbar.vala:58 src/ui/topbar.ui:212
msgid "Take Screenshot"
msgstr "Λήψη στιγμιοτύπου"

#: src/topbar.vala:62 src/ui/topbar.ui:216
msgid "Fullscreen"
msgstr "Πλήρης οθόνη"

#: src/ui/assistant.ui:26
msgid "Enter the network identifier of the remote desktop to connect to:"
msgstr ""
"Εισαγάγετε το αναγνωριστικό δικτύου του απομακρυσμένου υπολογιστή για "
"σύνδεση:"

#: src/ui/assistant.ui:53
msgid "Connection Type"
msgstr "Τύπος σύνδεσης"

#: src/ui/assistant.ui:63
msgid "RDP (standard for connecting to Windows)"
msgstr "RDP (πρότυπο για σύνδεση με Windows)"

#: src/ui/assistant.ui:72
msgid "VNC (standard for connecting to Linux)"
msgstr "VNC (πρότυπο για σύνδεση με Linux)"

#: src/ui/assistant.ui:90 src/ui/topbar.ui:201
msgid "Help"
msgstr "Βοήθεια"

#: src/ui/assistant.ui:100
msgid "Connect"
msgstr "Σύνδεση"

#: src/ui/auth-notification.ui:39
msgid "_Username"
msgstr "Ό_νομα χρήστη"

#: src/ui/auth-notification.ui:71
msgid "_Password"
msgstr "_Συνθηματικό"

#: src/ui/auth-notification.ui:108
msgid "Sign In"
msgstr "Σύνδεση"

#: src/ui/onboarding-dialog.ui:42 src/ui/window.ui:51
msgid "Welcome to Connections"
msgstr "Καλώς ήλθατε στις Συνδέσεις"

#: src/ui/onboarding-dialog.ui:43
msgid "Learn about how Connections works."
msgstr "Μάθετε πως να χρησιμοποιείτε τις Συνδέσεις."

#: src/ui/onboarding-dialog.ui:50
msgid "Use other desktops, remotely"
msgstr "Χρησιμοποιήστε άλλους υπολογιστές, από απόσταση"

#: src/ui/onboarding-dialog.ui:51
msgid ""
"Use Connections to view the screen of other desktops. You can control them "
"using the pointer and keyboard, too!"
msgstr ""
"Χρησιμοποιήστε τις Συνδέσεις για να προβάλετε την οθόνη άλλων επιτραπέζιων "
"υπολογιστών. Μπορείτε επίσης να τα ελέγξετε χρησιμοποιώντας το δείκτη και το "
"πληκτρολόγιο!"

#: src/ui/onboarding-dialog.ui:58
msgid "Connect to different operating systems"
msgstr "Συνδεθείτε σε διαφορετικά λειτουργικά συστήματα"

#: src/ui/onboarding-dialog.ui:59
msgid "Access Linux, Mac, and Windows desktops using Connections."
msgstr ""
"Αποκτήστε πρόσβαση σε επιτραπέζιους υπολογιστές Linux, Mac και Windows "
"χρησιμοποιώντας τις Συνδέσεις."

#: src/ui/onboarding-dialog.ui:66
msgid "Enable remote desktop before connecting"
msgstr "Ενεργοποιήστε τον απομακρυσμένο υπολογιστή πριν από τη σύνδεση"

#: src/ui/onboarding-dialog.ui:67
msgid ""
"Computers need to be set up for remote desktop before you can connect to "
"them."
msgstr ""
"Οι υπολογιστές πρέπει να ρυθμιστούν για απομακρυσμένη σύνδεση για να "
"μπορέσετε να συνδεθείτε σε αυτούς."

#: src/ui/onboarding-dialog.ui:74
msgid "We hope that you enjoy Connections!"
msgstr "Ελπίζουμε να απολαύσετε τις Συνδέσεις!"

#: src/ui/onboarding-dialog.ui:75
msgid "More information can be found in the help."
msgstr "Περισσότερες πληροφορίες μπορείτε να βρείτε στη βοήθεια."

#: src/ui/rdp-preferences.ui:9 src/ui/vnc-preferences.ui:9
msgid "Connection preferences"
msgstr "Προτιμήσεις σύνδεσης"

#: src/ui/rdp-preferences.ui:22 src/ui/vnc-preferences.ui:22
msgid "Address"
msgstr "Διεύθυνση"

#: src/ui/rdp-preferences.ui:37 src/ui/vnc-preferences.ui:37
msgid "Name"
msgstr "Όνομα"

#: src/ui/rdp-preferences.ui:53 src/ui/vnc-preferences.ui:108
msgid "Fit window"
msgstr "Ταίριασμα στο παράθυρο"

#: src/ui/rdp-preferences.ui:54 src/ui/vnc-preferences.ui:109
msgid "Original size"
msgstr "Αρχικό μέγεθος"

#: src/ui/topbar.ui:21
msgid "New"
msgstr "Νέο"

#: src/ui/topbar.ui:42
msgid "Application Menu"
msgstr "Μενού εφαρμογής"

#: src/ui/topbar.ui:70
msgid "Search"
msgstr "Αναζήτηση"

#: src/ui/topbar.ui:104
msgid "Go Back"
msgstr "Μετάβαση πίσω"

#: src/ui/topbar.ui:125
msgid "Display Menu"
msgstr "Μενού εμφάνισης"

#: src/ui/topbar.ui:149
msgid "Disconnect"
msgstr "Αποσύνδεση"

#: src/ui/topbar.ui:174
msgid "Keyboard shortcuts"
msgstr "Συντομεύσεις πληκτρολογίου"

#: src/ui/topbar.ui:197
msgid "Keyboard Shortcuts"
msgstr "Συντομεύσεις πληκτρολογίου"

#: src/ui/topbar.ui:205
msgid "About Connections"
msgstr "Περί του Συνδέσεις"

#: src/ui/topbar.ui:234
msgid "Ctrl + Alt + Backspace"
msgstr "Ctrl + Alt + Backspace"

#: src/ui/topbar.ui:241
msgid "Ctrl + Alt + Del"
msgstr "Ctrl + Alt + Del"

#: src/ui/topbar.ui:248
msgid "Ctrl + Alt + F1"
msgstr "Ctrl + Alt + F1"

#: src/ui/topbar.ui:255
msgid "Ctrl + Alt + F2"
msgstr "Ctrl + Alt + F2"

#: src/ui/topbar.ui:262
msgid "Ctrl + Alt + F3"
msgstr "Ctrl + Alt + F3"

#: src/ui/topbar.ui:269
msgid "Ctrl + Alt + F7"
msgstr "Ctrl + Alt + F7"

#: src/ui/vnc-preferences.ui:54
msgid "Display"
msgstr "Εμφάνιση"

#: src/ui/vnc-preferences.ui:59
msgid "View only"
msgstr "Μόνο προβολή"

#: src/ui/vnc-preferences.ui:66
msgid "Show local pointer"
msgstr "Εμφάνιση τοπικού δείκτη"

#: src/ui/vnc-preferences.ui:73
msgid "Enable audio"
msgstr "Ενεργοποίηση ήχου"

#: src/ui/vnc-preferences.ui:81
msgid "Bandwidth"
msgstr "Εύρος ζώνης"

#: src/ui/vnc-preferences.ui:88
msgid "High quality"
msgstr "Υψηλή ποιότητα"

#: src/ui/vnc-preferences.ui:89
msgid "Fast refresh"
msgstr "Γρήγορη ανανέωση"

#: src/ui/vnc-preferences.ui:100
msgid "Scale mode"
msgstr "Λειτουργία κλιμάκωσης"

#: src/ui/window.ui:52
msgid "Just hit the <b>+</b> button to make your first connection."
msgstr "Πατήστε το κουμπί <b>+</b> για να κάνετε την πρώτη σας σύνδεση."

#: src/vnc-connection.vala:149
msgid "Couldn’t parse the file"
msgstr "Αδυναμία ανάλυσης του αρχείου"

#. Translators: %s is a VNC file key
#: src/vnc-connection.vala:157 src/vnc-connection.vala:162
#: src/vnc-connection.vala:167 src/vnc-connection.vala:172
#, c-format
msgid "VNC File is missing key “%s”"
msgstr "Λείπει το κλειδί “%s” στο αρχείο VNC"

#~ msgid "Scaling"
#~ msgstr "Κλιμάκωση"

#~ msgid "GNOME Connections"
#~ msgstr "Συνδέσεις GNOME"

#~ msgid "A remote desktop client for the GNOME desktop environment."
#~ msgstr ""
#~ "Ένας πελάτης απομακρυσμένης επιφάνειας εργασίας για το περιβάλλον "
#~ "επιφάνειας εργασίας GNOME."

#~ msgid "Create a New Connection"
#~ msgstr "Δημιουργία Νέας Σύνδεσης"

#~ msgid ""
#~ "Enter a machine address to connect to. Address can begin with rdp:// or "
#~ "vnc://"
#~ msgstr ""
#~ "Εισαγάγετε μια διεύθυνση για σύνδεση. Οι διευθύνσεις μπορούν να "
#~ "ξεκινήσουν με rdp:// ή vnc://"

#~ msgid "Server"
#~ msgstr "Διακομιστής"

#~ msgid "Cancel"
#~ msgstr "Aκύρωση"

#~ msgid "Create"
#~ msgstr "Δημιουργία"

#~ msgid "A remote connection manager for the GNOME desktop"
#~ msgstr ""
#~ "Ένας διαχειριστής απομακρυσμένης σύνδεσης για το περιβάλλον εργασίας GNOME"

#~ msgid "— A simple application to access remote connections"
#~ msgstr "— Μια απλή εφαρμογή για πρόσβαση σε απομακρυσμένες συνδέσεις"

#~ msgid "VNC File is missing key “Port“"
#~ msgstr "Λείπει η «Θύρα» από το αρχείο VNC"

#~ msgid "VNC File is missing key “Username“"
#~ msgstr "Λείπει το «Όνομα χρήστη» από το αρχείο VNC"

#~ msgid "VNC File is missing key “Password“"
#~ msgstr "Λείπει το «Συνθηματικό» από το αρχείο VNC"
